var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://localhost:27017/line4';


var collection = MongoClient.connect(url).then(db => {
  console.log("Connected to database");
  return db.collection('fighters');
});

var getFighters = function(queryObj){
	console.log();
	return collection.then(fighters => fighters.find(queryObj).toArray());
};

function saveFighter(fighterToSave){
	return collection.then(function (fightersCollection){
		return fightersCollection.updateOne(
		{ 	login: fighterToSave.login,
			password: fighterToSave.password,
			rating: fighterToSave.rating
		}, fighterToSave,
		{upsert: true}
		);
	});
}

function updateFighter(fighterToSave){
	return collection.then(function (fightersCollection){
		return fightersCollection.updateOne(
		{login: fighterToSave.login}, fighterToSave,
		{upsert: true}
		);
	});
}

module.exports = {
	saveFighter, getFighters, updateFighter
};