
(function () {
    'use strict';

    angular
        .module('ModFour', [])
        .controller('CtrlFour', four);

    function four($scope) {
        $scope.player = "";
        $scope.playing = 0;
        $scope.finish = 0;
        $scope.winner = "";
        $scope.opponent = "";
        $scope.you = "";
        $scope.count = 0;

        $scope.ifLogged = 0;
        $scope.ifRegister = 0;
        $scope.table = [
            ["white", "white", "white", "white", "white", "white", "white"],
            ["white", "white", "white", "white", "white", "white", "white"],
            ["white", "white", "white", "white", "white", "white", "white"],
            ["white", "white", "white", "white", "white", "white", "white"],
            ["white", "white", "white", "white", "white", "white", "white"],
            ["white", "white", "white", "white", "white", "white", "white"]
        ];

        $(document).ready(function(){
            $('#loader').css('display','none');
            $('#wrapper').css('display','block');
        });

        $( ".container" ).click(function() {
            socket.emit("playerClick", $scope.table);
                //player: $scope.player});
        });
        socket.on('playerClick', function(data){
            $scope.$applyAsync(function () {
                $scope.table = data;
                //$scope.player = data.player;
            });
        });

        socket.on('playerMove', function(data){
            $scope.$applyAsync(function () {
                if (data === 'red'){
                    $scope.player = 'blue';
                } else {
                    $scope.player = 'red';
                }
            })
        });

        socket.on('playerChoose', function(player){
            $scope.$applyAsync(function () {
                $scope.player = player;
                if ($scope.player === 'red'){
                    $scope.you = 'blue'
                    $scope.opponent = 'red';
                } else if ($scope.player === 'blue'){
                    $scope.you = 'red';
                    $scope.opponent = 'blue';
                }
                $scope.playing = 1;
            });
        });

        socket.on('playerWin', function(player){
            $scope.$applyAsync(function(){
                $scope.winner = player;
                $scope.finish = 1;
            })
        });
        socket.on('playerReset', function(){
            $scope.$applyAsync(function(){
                $scope.finish = 0;
            })
        });
        $scope.loginObj = null;
        $scope.loginFunc = function(){
            $scope.loginObj = {
                'login': $scope.login,
                'password': $scope.password,
                'rating': 0
            }
            socket.emit('login', $scope.loginObj);
        }
        $scope.toggleRegister = function(){
            $scope.ifRegister = !$scope.ifRegister;
            console.log("Register" + $scope.ifRegister);
        };
        $scope.register = function(){
            console.log("You've rgistered from: " + $scope.login + $scope.password);
            $scope.toggleRegister();
            $scope.loginObj = {
                'login': $scope.login,
                'password': $scope.password,
                'rating': 0
            }
            socket.emit('register', $scope.loginObj);
        }
        socket.on('login', function(data){
            $scope.$applyAsync(function(){
                console.log("Got login from database!");
                $scope.fighters = data;
            });
        });
        socket.on('register', function(data){
            console.log("Got register from database!");
            $scope.loginFunc();
        });
        $scope.fighters;
        socket.on('sucLogin', function(data){
            $scope.$applyAsync(function(){
                    if (data.length){
                        console.log("Succesful login!");
                        $scope.ifLogged = 1;
                    } else {
                        console.log("Podales zle haslo albo login!");
                    }
            });
        });

        socket.on('playerRankUp', function(data){
            console.log("Got rank up from database!");
            console.log(data);
            $scope.$applyAsync(function(){
                $scope.fighters = data;    
            });    
        });
        socket.on('playerRankDown', function(data){
            $scope.fighters = data;
        });

        $scope.toggleGame = function(){
            $scope.playing = !$scope.playing;
        };

        $scope.choosePlayer = function(player) {
            socket.emit('playerChoose', player);
            $scope.player = player;
            if (player === "red"){
                $scope.you = "red";
                $scope.opponent = "blue";
            } else if (player === "blue"){
                $scope.you = "blue";
                $scope.opponent = "red";
            }
            $scope.playing = 1;
        };

        $scope.finishToggle = function() {
            $scope.finish = !$scope.finish;
            reset();
            $scope.winner = $scope.player;
            //$scope.playerSwap();
        };

        $scope.finishEnd = function() {
            $scope.finish = !$scope.finish;
            reset();
            socket.emit('playerReset');
            $scope.winner = $scope.player;
            //$scope.playerSwap();
        };

        $scope.playerSwap = function() {
            if ($scope.player === "red") {
                $scope.playerChange('blue');
            } else {
                $scope.playerChange('red');
            }
        };



        var reset = function () {
            for (var i = 0; i < 6; i++) {
                for (var j = 0; j < 7; j++) {
                    $scope.table[i][j] = "white";
                }
            }
        };
        $scope.playerChange = function (data) {
            $scope.player = data;
        };

        $scope.assign = function (data) {
            if ($scope.player !== $scope.you) return false;
            socket.emit('playerMove', $scope.player);
            var temp = 0;
            while (temp < 5 && $scope.table[temp + 1][data] === "white") {
                temp++;
            }
            if ($scope.table[temp][data] === "white" ){ //&& $scope.player === $scope.you) {
                $scope.table[temp][data] = $scope.player;
                check(temp, data, $scope.player);
                $scope.playerSwap();
            }
            socket.emit("playerClick", $scope.table);
        };

        var check = function (row, col, color) {
            var streak = 0,
                temp_row = row,
                temp_col = col;
            /**********************************************************************Prosta*/
            while (col >= 0 && col <= 6 && $scope.table[row][col] === color) {
                streak++;
                col++;
            }
            col = temp_col - 1;
            while (col >= 0 && $scope.table[row][col] === color) {
                streak++;
                col--;
            }
            if (streak > 3) {
                socket.emit('playerWin', $scope.player);
                $scope.loginObj = {
                'login': $scope.login,
                'password': $scope.password
            }
                socket.emit('playerRankUp', $scope.loginObj);
                $scope.finishToggle();
            }
            /**********************************************************************Prosta*/
            row = temp_row;
            col = temp_col;
            streak = 0;

            while (row >= 0 && row <= 5 && $scope.table[row][col] === color) {
                streak++;
                row++;
            }
            row = temp_row - 1;
            while (row >= 0 && $scope.table[row][col] === color) {
                streak++;
                row--;
            }
            if (streak > 3) {
                socket.emit('playerWin', $scope.player);
                $scope.loginObj = {
                'login': $scope.login,
                'password': $scope.password
            }
                socket.emit('playerRankUp', $scope.loginObj);
                $scope.finishToggle();
            }
            /*********************************************************************Ukos 1*/
            row = temp_row;
            col = temp_col;
            streak = 0;
            while (row >= 0 && col >= 0 && row <= 5 && col <= 6 && $scope.table[row][col] === color) {
                streak++;
                row++;
                col++;
            }
            row = temp_row - 1;
            col = temp_col - 1;
            while (row >= 0 && col >= 0 && $scope.table[row][col] === color) {
                streak++;
                row--;
                col--;
            }
            if (streak > 3) {
                socket.emit('playerWin', $scope.player);
                $scope.finishToggle();
            }
            /*********************************************************************Ukos 2*/
            row = temp_row;
            col = temp_col;
            streak = 0;
            while (row >= 0 && col >= 0 && row <= 5 && col <= 6 && $scope.table[row][col] === color) {
                streak++;
                row--;
                col++;
            }

            row = temp_row + 1;
            col = temp_col - 1;
            while (row >= 0 && col >= 0 && row <= 5 && col <= 6 && $scope.table[row][col] === color) {
                streak++;
                row++;
                col--;
            }
            if (streak > 3) {
                socket.emit('playerWin', $scope.player);
                $scope.loginObj = {
                'login': $scope.login,
                'password': $scope.password
            }
                socket.emit('playerRankUp', $scope.loginObj);
                $scope.finishToggle();
            }
        };
    }

})();