var express = require("express");
var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);

var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/line4';
var fighters = require("./public/fighters");


app.use(express.static(__dirname + "/public"));
server.listen(80);

app.get('/', function(req, res){
    res.sendfile(__dirname + '/public/index.html');
});
var count = 0;
var clients = io.sockets.connected;

io.on('connection', function(socket ){
    console.log("connected user: " + count);
    count++;
    socket.on('chat message', function(res, req){
        socket.broadcast.emit('chat message', res);
        //res = count;
        console.log("Thats the res" + res);
    });
    socket.on('login', function(data){
        socket.join(data.login);
        fighters.getFighters()
            .then(fighters => io.to(data.login).emit('login', fighters));
        //fighters.getFighters({'login': data.login}).then(fighters => io.to(data.login).emit('sucLogin', fighters));
        fighters.getFighters({'login': data.login, 'password': data.password}).then(function(fighters){
          io.to(data.login).emit('sucLogin', fighters);
        });
    });
    socket.on('playerRankUp', function(data){
        console.log("Database got rank up!");
        fighters.getFighters({'login': data.login})
            .then(function(fighter){
                fighter[0].rating += 20;
                console.log('fighter', fighter);
                fighterObj = {
                    'login': data.login,
                    'password': data.password,
                    'rating': fighter[0].rating
                }
                console.log('figherOBj', fighterObj);
                fighters.updateFighter(fighterObj);
            }).then(function(){
                return fighters.getFighters();
            })
            .then(function(fighterList){
                 console.log('figherList', fighterList);
                 io.emit('playerRankUp', fighterList);
            })
            .catch(err => console.log(err));
                /*
                .then(fighter => fighters.getFighters())
                .then(function(fighters){
                    io.emit('playerRankUp', fighters);
                });*//*
        fighters.getFighters({}).then(function(fighters){
            io.emit('playerRankUp', fighters);
        });*/
    });
    socket.on('register', function(data){
        fighters.saveFighter(data);
        io.to(data.login).emit('register');
    });
    socket.on('playerMove', function(data){
        io.emit('playerMove', data);
    });
    socket.on('playerClick', function(data){
        io.emit('playerClick', data);
    });
    socket.on('playerChoose', function(player){
        socket.broadcast.emit('playerChoose', player);
    });
    socket.on('playerWin', function(player){
        io.emit('playerWin', player);
    });
    socket.on('playerReset', function(){
        io.emit('playerReset');
    });
});



app.use(function(err, req, res, next) {
    console.log(err);
});